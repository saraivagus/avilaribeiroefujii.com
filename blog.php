<?php

    require_once("sistema/conecta.php");
    require_once("sistema/includes/limita-texto.php");

?>
<html lang="zxx">
  <head>
    <meta charset="utf-8">
    <title>Ávila ribeiro e Fujii</title>
<meta property="og:locale" content="pt_BR" />
	<meta property="og:site_name" content="Ávila Ribeiro e Fujii" /> <!-- website name -->
	<meta property="og:site" content="http://www.avilaribeiroefujii.com" /> <!-- website link -->
	<meta property="og:title" content="Ávila Ribeiro e Fujii"/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="
Acreditamos ser o trabalho ético a chave para a resolução de todos os problemas. Aliando-o à paixão do jovem staff de advogados, Ávila Ribeiro e Fujii Advogados Associados preza pelo desempenho e a efetivação de parcerias, compreendidas como indispensáveis para a prosperidade em um mundo cada vez mais globalizado e de relacionamentos múltiplos e dinâmicos." /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="http://www.avilaribeiroefujii.com/img/facebook.png" />
	<meta property="og:url" content="http://avilaribeiroefujii.com" />
	<meta property="og:type" content="article" />
    <!-- favicon -->
    <link href="img/favicon.png" rel="icon" sizes="32x32" type="image/png">
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- black CSS -->
    <link href="css/animated-black.css" rel="stylesheet">
    <link href="css/black-style.css" rel="stylesheet">
    <link href="css/queries-black.css" media="all" rel="stylesheet" type="text/css">
  </head>
  <body>

    <!-- preloader -->
    <div class="bg-preloader"></div>
    <div class="preloader">
      <div class="mainpreloader">
        <img alt="preloaderlogo" src="img/logo.png"> <span class="logo-preloader">Aguarde...</span>
      </div>
    </div>
    <!-- preloader end -->

    <!-- content wraper -->
    <div class="content-wrapper">
      <!-- nav -->

      <div class="navbar navbar-default navbar-fixed-top onStep" data-animation="fadeInDown" data-time="0">
        <div class="container">

          <!-- menu mobile display -->
           <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
           <span class="icon icon-bar"></span>
           <span class="icon icon-bar"></span>
           <span class="icon icon-bar"></span></button>

          <!-- logo -->
          <a class="navbar-brand" href="index.html"><img alt="" src="img/logo.png"></a>

          <!-- mainmenu start -->
          <div class="menu-init" id="main-menu">
             <nav>
              <ul>
                <li><a href="index.html" >Início</a></li>
                
                 <li><a href="https://www.facebook.com/avilaribeiroefujii" target="_blank"><span class="ti-facebook"></span></li></a>
              </ul>
            </nav>
          </div>
          <!-- mainmenu end -->

        </div>
        <!-- .container -->
      </div>
      <!-- nav end -->

        <!-- subheader -->
        <section id="subheader">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">


                    </div>
                </div>
            </div>
        </section>
        <!-- subheader end -->


            <!-- section -->
            <section id="content" aria-label="section-blogg">
            <div class="container">
            <div class="row">
 <h1>Publicações</h1>
                  <!-- left content -->
                  <div class="col-md-12 col-sm-12">
                  <div class="onStep" data-animation="fadeInUp" data-time="300">
<?php
     if(empty($_GET['pg'])){}
      else{ $pg = $_GET['pg'];
          if(!is_numeric($pg)){
             echo '<script language "JavaScript">
      location.href="postagem.php";
      </script>';

      }
      }

      if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}

      $quantidade = 8;
      $inicio = ($pg*$quantidade) - $quantidade;

        $sql = "SELECT * FROM tb_postagens WHERE exibir='Sim' ORDER BY id DESC LIMIT $inicio, $quantidade";
        try {
            $resultado = $connection->prepare($sql);
            $resultado->execute();
            $contar = $resultado->rowCount();

            if($contar>0){

                while($exibe = $resultado->fetch(PDO::FETCH_OBJ)){
            ?>
            
                     <!-- article -->
                     <article>

                        <div class="post-image">
                           <img class="img-responsive" width="300px" src="../upload/<?php echo $exibe->imagem;?>" alt="<?php echo $exibe->titulo;?>" title="<?php echo $exibe->titulo;?>" />
                           <div class="post-heading">
                              <h3><?php echo $exibe->titulo;?></h3>
                           </div>

                        </div>
                       <p> <?php echo limitarTexto($exibe->descricao, $limite=380)?></p>
                        <div class="bottom-article">
                           <ul class="meta-post">
                              <li><a href="#"><strong><?php echo $exibe->data;?></strong></a>
                              </li>
                           </ul>
                           <a href="blog-post.php?id=<?php echo $exibe->id;?>" class="btn pull-right">Leia o artigo completo</a>
                        </div>
                     </article>
                     <!-- article end -->
 <?php
                } // while
                }else{
                    echo '<li>não existe post cadastrado.</li>';
                    }
                }catch(PDOException $erro){
                    echo $erro;
                        }
                        ?>
                     <!-- article -->

                     <!-- article end -->
<style>
.paginas {width: 100%;padding: 10px 0;text-align: center;background: #fff;height: auto;margin:10px auto;}
.paginas a {width: auto;padding: 4px 10px;background: #ccc;color:#333;margin: 0px 4px;text-decoration:none; font-family: 'Raleway', sans-serif;font-size:13px}
.paginas a:hover{text-decoration: none;background: #00BA8b;color:#fff}
                    <?php
  if(isset($_GET['pg'])){
    $num_pg = $_GET['pg'];
  }else{$num_pg = 1;}
?>

.paginas a.ativo<?php echo $num_pg;?> {background: #00BA8b;color:#fff;}
</style>
<!-- inicio botoes -->

<?php
  $sql = "SELECT * FROM tb_postagens";
  try{
    $result = $connection->prepare($sql);
    $result->execute();
    $totalRegistros = $result->rowCount();
  }catch(PDOException $e){
    echo $e;
  }

    if($totalRegistros <=$quantidade){}
    else{
      $paginas = ceil($totalRegistros/$quantidade);
      if($pg > $paginas){
      echo '<script language "JavaScript">
      location.href="blog.php"; </script>'; }
      $links = 5;

      if(isset($i)){}
        else{$i = '1';}
?>


<div class="paginas">
<a href="blog.php?pg=1">Primeira Página</a>

<?php
if(isset($_GET['pg'])){
  $num_pg = $_GET['pg'];
}

for($i = $pg-$links; $i <= $pg-1; $i++){
  if($i<=0){}
  else{

  ?>
  <a href="blog.php?pg=<?php echo $i;?>" class="ativo<?php echo $i;?>"><?php echo $i;?></a>

  <?php  }}?>

<a href="blog.php?pg=<?php echo $pg;?>" class="ativo<?php echo $i;?>"> <?php echo $pg;?></a>

<?php

  for($i = $pg+1; $i <= $pg+$links; $i++){
    if($i>$paginas){}
    else{
      ?>

<a href="blog.php?pg=<?php echo $i;?>" class="ativo<?php echo $i;?>"><?php echo $i;?></a>
  <?php }}?>
<a href="blog.php?pg=<?php echo $paginas;?>"> Última Página </a>
<?php  } ?>
                    <!-- next prev blog end -->

                     </div>
                  </div>
                  <!-- left content end -->

                  <!-- right content -->

                  </div>
                  <!-- right content end -->
               </div>
             </div>
            </section>
            <!-- section end -->


      <!-- footer -->
      <section class="footer" aria-label="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                        <div class="onStep" data-animation="fadeInRight" data-time="0">
                            Ávila Ribeiro e Fujii &copy; Copyright 2017 by gsaraiva.com
                        </div>
                        </div>

                    </div>
                </div>
      </section>
      <!-- footer end -->

      <!-- ScrolltoTop -->
      <div id="totop">
        <span class="ti-angle-up"></span>
      </div>

</div>
<!-- content wraper end -->

    <!-- plugin JS -->
    <script src="plugin/pluginsblack.js" type="text/javascript"></script>
    <!-- black JS -->
    <script src="js/black.js" type="text/javascript"></script>

  </body>
</html>
