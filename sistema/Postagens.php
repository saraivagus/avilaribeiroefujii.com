<?php include("includes/Header.php");?>
<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="la-anim-1"></div>
    </div>
    <!-- /Preloader -->
    <div class="wrapper theme-1-active pimary-color-pink">
        <?php include("includes/TopBarMenu.php");?>
        <?php include("includes/LeftSideBar.php");?>
        <?php include("includes/RightSideBar.php");?>
        <?php include("includes/Postagens.php");?>
    </div>
    <?php include("includes/Footer.php");?>
</body>
</html>
