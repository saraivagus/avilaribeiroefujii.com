
<!DOCTYPE html>
<html lang="br">  
 <head>
    <meta charset="utf-8">
    <title>Recuperar senha - WVA System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">

			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.php">
				Recuperar senha
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="">						
						<a href="index.php" class="">
							Fazer login
						</a>
						
					</li>
					<li class="">						
						<a href="index.html" class="">
							<i class="icon-chevron-left"></i>
							Acessar o site
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->

<div class="account-container register">
	
	<?php  
	if(isset($_POST['recuperar'])){
		include("conexao/conecta.php");
			$email    = utf8_decode (addslashes(strip_tags(trim($_POST['email']))));
			// VERIFICA SE O EMAIL ESTÁ NO BANCO DE DADOS
			$select = "SELECT * from login WHERE email='$email' ";
		try{
			$result = $connection->prepare($select);
			// $result->bindValue(':email', $email, PDO::PARAM_STR);
			$result->execute();
			$contar = $result->rowCount();
			if($contar>0){
				foreach($connection->query($select) as $show);
				$nomeUser    = $show['nome'];
				$emailUser   = $show['email'];
				$usuarioUser = $show['usuario'];
				$senhaUser   = $show['senha'];
				require_once('envia/PHPMailer/class.phpmailer.php');
			
			$Email = new PHPMailer();
			$Email->SetLanguage("en");
			$Email->IsSMTP(); // Habilita o SMTP 
			$Email->SMTPAuth = true; //Ativa e-mail autenticado
			$Email->SMTPDebug = false;       // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
			$Email->SMTPSecure = 'ssl';
			$Email->Host = "a2plcpnl0516.prod.iad2.secureserver.net"; // Endereço do servidor SMTP
			$Email->Port = '465 '; // Porta de envio
			$Email->Username = 'senhas@gsaraiva.com'; //e-mail que será autenticado
			$Email->Password = 'RlvFk_iZzk$H'; // senha do email
			// ativa o envio de e-mails em HTML, se false, desativa.
			$Email->IsHTML(true); 
			// email do remetente da mensagem
			$Email->From = 'senhas@gsaraiva.com';
			// nome do remetente do email
			$Email->FromName = utf8_decode($email);
			// Endereço de destino do emaail, ou seja, pra onde você quer que a mensagem do formulário vá?
			$Email->AddReplyTo($email, 'Gustavo Saraiva');
			$Email->AddAddress("senhas@gsaraiva.com"); // para quem será enviada a mensagem
			// informando no email, o assunto da mensagem
			$Email->Subject = "(Recuperacao de senha)";
			// Define o texto da mensagem (aceita HTML)
			$Email->Body .= "Dados para acesso ao sistema:<br />
							 <strong>Nome:</strong> $nomeUser<br />
							 <strong>E-mail:</strong> $emailUser<br />
							 <strong>Usuario:</strong> $usuarioUser<br />
							 <strong>Senha:</strong> $senhaUser<br /><br />
							";
			// verifica se está tudo ok com oa parametros acima, se nao, avisa do erro. Se sim, envia.
			if(!$Email->Send()){
					echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>Erro ao enviar</strong> Contate o administrador.
                      </div>';
				echo "Erro: " . $Email->ErrorInfo;
			}else{
					echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>E-mail enviado com sucesso!</strong> <br>Por favor verifique seu email.
                      </div>';
			}
			}else{
				echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>Erro ao recuperar sua senha</strong><br>O email digitado não consta em nosso sistema.
                      </div>';
			}
		}catch(PDOException $e){
			echo $e;
		}
			
			
}
?>

	<div class="content clearfix">
		<form action="#" method="post" enctype="multipart/form-data">
			<h1>Recuperar senha</h1>			
			<div class="login-fields">
				<p>Digite o e-mail cadastrado no sistema:</p>

				<div class="field">
					<label for="email">Email Address:</label>
					<input type="text" id="email" name="email" value="" placeholder="Email" class="login"/>
				</div> <!-- /field -->
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				<input type="submit" value="Recuperar senha" class="button btn btn-primary btn-large" name="recuperar">
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	Deseja logar-se? <a href="index.php">Clique aqui para entrar</a>
</div> <!-- /login-extra -->


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

 </html>
