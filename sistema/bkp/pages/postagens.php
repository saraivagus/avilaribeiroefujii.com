
<link rel="stylesheet" type="text/css" href="../css/style.css" media="all">

<div class="main">
  <div class="main-inner">
    <div class="container">
     <div class="row">
  <div class="span12">
<?php 

    // excluir post
    if(isset($_GET['delete'])){
      $id_delete = $_GET['delete'];

      if($nivelLogado ==0){
        header("Location: home.php");exit;
      }
      if($nivelLogado ==1){

    

      // seleciona a imagem
      $seleciona = "SELECT * FROM tb_postagens WHERE id=:id_delete";
      try{
        $result = $connection->prepare($seleciona);
        $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
          $loop = $result->fetchAll();
          foreach ($loop as $exibir){
          }
           $fotoDeleta = $exibir['imagem'];
           $arquivo = "../upload/" .$fotoDeleta;
           unlink($arquivo);


      // exclui o registro
            $seleciona = "DELETE FROM tb_postagens WHERE id=:id_delete";
            try{
              
              $result = $connection->prepare($seleciona);
              $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
              $result->execute();
              $contar = $result->rowCount();
              if($contar>0){
                	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
								  <strong>Sucesso!</strong> O post foi excluido com sucesso.
                			      </div>';
              }else{
                echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Erro!</strong> Não foi possível excluir o post.
                    		  </div>';
              }
                }catch (PDOWException $erro){ echo $erro;}

        }else{
   echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Erro!</strong> Post já excluido ou não existe.
                    		  </div>';
        }

      }catch (PDOWException $erro){ echo $erro;}
      
    }else{
       echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Erro!</strong> Seu nível não permite a exclusão de registros.
                    		  </div>';
    }


    }

?>
	 
</div>


            <div class="span12">	      		
	          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Visualizar Posts</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th> ID</th>
                    <th> Título da Postagem </th>
                    <th> Data</th>
                    <th> Imagem</th>
                    <th> Exibição </th>
                    <th> Resumo</th>
                    <th class="td-actions"> </th>
                  </tr>
                </thead>
                <tbody>


<?php 
  include("functions/limita-texto.php");

      if(empty($_GET['pg'])){}
      else{ $pg = $_GET['pg'];
          if(!is_numeric($pg)){
             echo '<script language "JavaScript"> 
      location.href="home.php?acao=postagens"; 
      </script>';

      }
      }
      
      if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}
    
      $quantidade = 10;
      $inicio = ($pg*$quantidade) - $quantidade;

      $select = "SELECT * FROM tb_postagens ORDER BY id DESC LIMIT $inicio, $quantidade";
      $contagem =$inicio + 1;

		try{
			$result = $connection->prepare($select);
			$result->execute();
			$contar = $result->rowCount();
			if($contar>0){
        while($mostrar = $result->FETCH(PDO::FETCH_OBJ)){
       
                ?>

                  <tr>
                    <td> <?php echo $contagem++;?> </td>
                    <td> <?php echo $mostrar->titulo;?> </td>
                    <td> <?php echo $mostrar->data;?> </td>
                    <td> <img src="../upload/<?php echo $mostrar->imagem;?>" width="80">  </td>
                    <td> <?php echo $mostrar->exibir;?></td>
                    <td> <?php echo limitarTexto($mostrar->descricao, $limite=100)?> </td>
                    <td class="td-actions"><a href="home.php?acao=edt-postagem&id=<?php echo $mostrar->id;?>" class="btn btn-small btn-success">
                    <i class="btn-icon-only icon-edit"> </i></a>
                    
                    <a href="home.php?acao=postagens&pg=<?php echo $pg;?>&delete=<?php echo $mostrar->id;?>" class="btn btn-danger btn-small"  onClick="return confirm('Deseja realmente excluir o post?')">
                    <i class="btn-icon-only icon-remove"> </i></a></td>
                  </tr>
                  <?php      
        }
			}else{
				echo '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>Aviso</strong> Não existem posts cadastrados em nosso banco de dados.
                      </div>';
			}
		}catch(PDOException $e){
			echo $e;
    }
                  ?>
                  
                
                </tbody>
                
              </table>
            </div>
            <!-- /widget-content --> 
<style>
.paginas {
	width: 100%;
	padding: 10px 0;
	text-align: center;
	background: #fff;
	height: auto;
	margin:10px auto;

}
.paginas a {
	width: auto;
	padding: 4px 10px;
	background: #ccc;
	color:#333;
	margin: 0px 4px;
}
.paginas a:hover{
	text-decoration: none;
	background: #00BA8b;
	color:#fff
}

<?php 
  if(isset($_GET['pg'])){
    $num_pg = $_GET['pg'];
  }else{$num_pg = 1;}
?>

.paginas a.ativo<?php echo $num_pg;?> {
  	background: #00BA8b;
    color:#fff;

}
</style>
<!-- inicio botoes -->

<?php
  $sql = "SELECT * FROM tb_postagens";
  try{
    $result = $connection->prepare($sql);
    $result->execute();
    $totalRegistros = $result->rowCount();
  }catch(PDOException $e){
    echo $e;
  }

    if($totalRegistros <=$quantidade){}
    else{
      $paginas = ceil($totalRegistros/$quantidade);
      if($pg > $paginas){
      echo '<script language "JavaScript"> 
      location.href="home.php?acao=postagens"; </script>'; }
      $links = 5;
      
      if(isset($i)){}
        else{$i = '1';}
?>


<div class="paginas">
<a href="home.php?acao=postagens&pg=1">Primeira Página</a>

<?php
if(isset($_GET['pg'])){
  $num_pg = $_GET['pg'];
}

for($i = $pg-$links; $i <= $pg-1; $i++){
  if($i<=0){}
  else{ 
  
  ?>
  <a href="home.php?acao=postagens&pg=<?php echo $i;?>" class="ativo<?php echo $i;?>"><?php echo $i;?></a>
  
  <?php  }}?>

<a href="home.php?acao=postagens&pg=<?php echo $pg;?>" class="ativo<?php echo $i;?>"> <?php echo $pg;?></a>

<?php 

  for($i = $pg+1; $i <= $pg+$links; $i++){
    if($i>$paginas){}
    else{
      ?>

<a href="home.php?acao=postagens&pg=<?php echo $i;?>" class="ativo<?php echo $i;?>"><?php echo $i;?></a>
  <?php }}?>
<a href="home.php?acao=postagens&pg=<?php echo $paginas;?>"> Última Páginas </a>
<?php  } ?>
<!-- fim botoes -->
</div>
          </div>
          <!-- /widget --> 
      		</div><!-- span 12 -->

    </div><!-- row -->        
          
          
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="editor/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>