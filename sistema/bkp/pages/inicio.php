<div class="main">
  <div class="main-inner">
    <div class="container">
     <div class="row">
            <div class="span12">
              <?php 
                if(isset($_GET['acao'])){
	        	    	$acao = $_GET['acao'];
           
	        	      	if($acao=='welcome'){
                      echo 
                 '<div class="alert alert-info">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>Olá '.$nomeLogado.',</strong> bem-vindo ao sistema!
               </div>';
                    }
                }
               ?>
      		</div>
            <div class="span12">	      		
	      		<div id="target-1" class="widget">	      			
	      			<div class="widget-content">	      				
			      		<h1>Notícias</h1>			      		
			      		<p> Olá, como podem ver o sistema foi mudado para melhor performance!</p>
                <p> Já está também sendo preparado para inclusão de conteúdo direto no site como conversado anteriormente em reunião! </p>
                <p> Logo disponibilizarei o sistema com todas ferramentes necessárias!</p>
                <p> Para acessar seus arquivos, basta clicar na aba Arquivos > Visualizar!</p>
                <p> Qualquer dúvidas estou a disposição
                <p> Atenciosamente, <br>
                 Gustavo Saraiva </p>
		      		</div> <!-- /widget-content -->
	      		</div> <!-- /widget -->
      		</div><!-- span 12 -->

    </div><!-- row -->      
   
<?php 
    // excluir post
    if(isset($_GET['delete'])){
      $id_delete = $_GET['delete'];
  
      // seleciona a imagem
      $seleciona = "SELECT * FROM tb_postagens WHERE id=:id_delete";
      try{
        $result = $connection->prepare($seleciona);
        $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
          $loop = $result->fetchAll();
          foreach ($loop as $exibir){
          }
           $fotoDeleta = $exibir['imagem'];
           $arquivo = "../upload/" .$fotoDeleta;
           unlink($arquivo);


      // exclui o registro
            $seleciona = "DELETE FROM tb_postagens WHERE id=:id_delete";
            try{
              
              $result = $connection->prepare($seleciona);
              $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
              $result->execute();
              $contar = $result->rowCount();
              if($contar>0){
                	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
								  <strong>Sucesso!</strong> O post foi excluido com sucesso.
                			      </div>';
              }else{
                echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Erro!</strong> Não foi possível excluir o post.
                    		  </div>';
              }
                }catch (PDOWException $erro){ echo $erro;}

        }else{
   echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Erro!</strong> Post já excluido ou não existe.
                    		  </div>';
        }

      }catch (PDOWException $erro){ echo $erro;}



    }

?>
	

           <div class="widget widget-table action-table">
           
            <div class="widget-header"> 
            <i class="icon-th-list"></i>
              <h3>Últimos Posts</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                       <th> ID</th>
                    <th> Título da Postagem </th>
                    <th> Data</th>
                    <th> Resumo</th>
                    <th class="td-actions"> </th>
                    
                    
                  </tr>
                </thead>
                <tbody>


<?php 
  include("functions/limita-texto.php");
                $select = "SELECT * from tb_postagens ORDER BY id DESC LIMIT 10";
                $contagem =1;
		try{
			$result = $connection->prepare($select);
			$result->execute();
			$contar = $result->rowCount();
			if($contar>0){
        while($mostrar = $result->FETCH(PDO::FETCH_OBJ)){
       
                ?>

                  <tr>
                    <td> <?php echo $contagem++;?> </td>
                    <td> <?php echo $mostrar->titulo;?> </td>
                    <td> <?php echo $mostrar->data;?> </td>
                    <td> <?php echo limitarTexto($mostrar->descricao, $limite=100)?> </td>
                    <td class="td-actions"><a href="home.php?acao=edt-postagem&id=<?php echo $mostrar->id;?>" class="btn btn-small btn-success"><i class="btn-icon-only icon-edit"> </i></a>
                    <a href="home.php?delete=<?php echo $mostrar->id;?>" class="btn btn-danger btn-small" onClick="return confirm('Deseja realmente excluir o post?')"><i class="btn-icon-only icon-remove"> </i></a></td>
                  </tr>
                  <?php      
        }
			}else{
				echo '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>Aviso</strong> Não existem posts cadastrados em nosso banco de dados.
                      </div>';
			}
		}catch(PDOException $e){
			echo $e;
    }
                  ?>
                  
                
                </tbody>
                
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
          
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->