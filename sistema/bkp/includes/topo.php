<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php">Ambiente Seguro</a>
      <div class="nav-collapse">
         <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <?php if($nivelLogado ==1){?>
          <i class="icon-cog"></i> Opções <b class="caret"></b></a>
            <?php }?> 
            <ul class="dropdown-menu">
              <li><a href="../blog.php" target="_blank">Visualizar Site</a></li>
       
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">

          <i class="icon-user"></i> <?php echo $nomeLogado;?> <b class="caret"></b></a>

            <ul class="dropdown-menu">
             <!-- <li><a href="javascript:;">Perfil</a></li> -->
              <li><a href="?sair">Sair</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="pesquisar">
        </form> 
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
   
    <?php if(isset($_GET['acao'])){ $acao = $_GET['acao'];}else{$acao ='home';}?>
      <ul class="mainnav">

        <li <?php if($acao =="welcome" || ($acao =="home")){echo 'class="active"';} ?> > <a href="index.php"><i class="icon-dashboard"></i><span>Homepage</span> </a> </li>
<!-- CADASTRAR POSTS NO SITE  -->
        <?php if($nivelLogado ==1){?>

        <li class="<?php if($acao =="postagens" || ($acao =="cadastro-post")){ echo "active";}?> dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
        <i class="icon-file"></i><span>Postagens</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="home.php?acao=postagens">Visualizar</a></li>

             <?php if($nivelLogado ==1){?>
            <li><a href="home.php?acao=cadastro-post">Cadastrar</a></li>
              <?php }?> 
          </ul>
        </li>
        <?php }?> 

         <?php if($nivelLogado ==0){?>

        <li class="<?php if($acao =="arquivos" || ($acao =="arquivos")){ echo "active";}?> dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
        <i class="icon-user"></i><span>Arquivos</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="home.php?acao=arquivos">Visualizar</a></li>
         
          </ul>
        </li>
         <?php }?>

         <?php if($nivelLogado ==1){?>
      <li class="<?php if($acao =="arquivos" || ($acao =="arquivos")){ echo "active";}?> dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
        <i class="icon-user"></i><span>Arquivos</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="home.php?acao=arquivos">Visualizar</a></li>
         
          </ul>
        </li>
         <?php }?>

      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->