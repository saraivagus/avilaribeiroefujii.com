<?php
ob_start();
session_start();
if(isset($_SESSION['usuariosistema']) && isset($_SESSION['senhasistema'])){
	header("Location:home.php");exit;
}
include("conecta.php");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Ávila Ribeiro e Fujii - Sistema Integrado</title>
		<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- vector map CSS -->
		<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>



		<!-- Custom CSS -->
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->

		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
						<span class="brand-text">Ávila Ribeiro e Fujii</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Não tem uma conta?</span>
					<a class="inline-block btn btn-info  btn-rounded btn-outline" href="#">Cadastrar</a>
				</div>
				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Faça o login</h3>
											<?php
	if(isset($_GET['acao'])){

		if(!isset($_POST['logar'])){
			$acao = $_GET['acao'];
			if($acao=='negado'){
			echo '<div class="alert alert-danger alert-dismissable alert-style-1">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="zmdi zmdi-block"></i>Faça o login para acessar!
									</div>';
						}
		}


	}
	if(isset($_POST['logar'])){

		//Recuperar Dados do Form
		$usuario = trim(strip_tags($_POST['usuario']));
		$senha = trim(strip_tags($_POST['senha']));

		//Selecionar Banco de Dados
		$select = "SELECT * from login WHERE BINARY usuario=:usuario AND BINARY senha=:senha";

		try{
			$result = $connection->prepare($select);
			$result->bindParam(':usuario', $usuario, PDO::PARAM_STR);
			$result->bindParam(':senha', $senha, PDO::PARAM_STR);
			$result->execute();
			$contar = $result->rowCount();
			if($contar>0){
				$usuario = $_POST['usuario'];
				$senha = $_POST['senha'];
				$_SESSION['usuariosistema'] = $usuario;
				$_SESSION['senhasistema'] = $senha;
				$loginsucesso = '<div class="alert alert-success alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-check"></i>Yay! Logado com sucesso!
										</div>';
				header("Refresh:3,home.php?acao=welcome");
			}else{
				$loginerro = '<div class="alert alert-danger alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-block"></i>Opps! Dados incorretos
										</div>';
			}
		}catch(PDOException $e){
			echo $e;
		}
	}//Se Clicar no Botão Entra no Sistema
	?>
											<h6 class="text-center nonecase-font txt-grey">Insira seus dados abaixo</h6>
										</div>
										<?php echo $loginsucesso; ?>
										<?php echo $loginerro; ?>
										<div class="form-wrap">
											<form action="#" method="post" enctype="multipart/form-data">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Usuário</label>
													<input type="text" class="form-control" required="" id="username" name="usuario" placeholder="Insira seu usuário">
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Senha</label>
													<div class="clearfix"></div>
													<input type="password" class="form-control" required="" id="password" name="senha" placeholder="Insira sua senha">
												</div>

												<div class="form-group">

													<div class="clearfix"></div>
												</div>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info  btn-rounded" name="logar">Entrar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				</div>

			</div>
			<!-- /Main Content -->

		</div>
		<!-- /#wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>

		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script>
	</body>
</html>
