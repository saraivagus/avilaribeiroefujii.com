<div class="fixed-sidebar-left">
  <ul class="nav navbar-nav side-nav nicescroll-bar">
    <li class="navigation-header">
      <span>Main</span>
      <i class="zmdi zmdi-more"></i>
    </li>
    <li>
      <a href="home.php" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-home mr-20"></i><span class="right-nav-text">Dashboard</span></div></a>
    </li>
    <li>
      <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="zmdi zmdi-layers mr-20"></i><span class="right-nav-text">Postagens</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
      <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
        <li>
          <a href="AdicionarPostagem.php">Cadastrar</a>
        </li>
        <li>
          <a href="VerPostagem.php">Visualizar</a>
        </li>

      </ul>
    </li>
    				<li><hr class="light-grey-hr mb-10"/></li>
<li class="navigation-header">
					<span>Arquivos</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
<li>
      <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="zmdi zmdi-archive mr-20"></i><span class="right-nav-text">Arquivos</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
      <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">

        <li>
          <a href="VerPostagem.php">Visualizar</a>
        </li>

      </ul>
    </li>
<!--
    <li><hr class="light-grey-hr mb-10"/></li>
    <li class="navigation-header">
      <span>configurações</span>
      <i class="zmdi zmdi-more"></i>
    </li>
    <li>
      <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="zmdi zmdi-google-pages mr-20"></i><span class="right-nav-text">Usuários</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>

    </li> -->


  </ul>
</div>
