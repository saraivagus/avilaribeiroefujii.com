<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="dist/js/tinymce-data.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script src="vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="vendors/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="dist/js/form-picker-data.js"></script>
<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
<script src="dist/js/dropdown-bootstrap-extended.js"></script>
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script src="dist/js/init.js"></script>
