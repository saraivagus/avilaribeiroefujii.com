<script type="text/javascript">
jQuery(function($){
   $("#date").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
});
</script>
<div class="page-wrapper">
    <div class="container-fluid pt-25">


<div class="col-sm-12">
  <div class="panel panel-default card-view">
    <div class="panel-heading">
      <div class="pull-left">
        <h6 class="panel-title txt-dark">Cadastrar novo Post</h6>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-wrapper collapse in">
      <div class="panel-body">
        <div class="form-wrap">
          <?php
					 if(isset($_POST['cadastrar'])){

						 $titulo 	= trim(strip_tags($_POST['titulo']));
						 $data 		= trim(strip_tags($_POST['data']));
						 $exibir 	= trim(strip_tags($_POST['exibir']));
						 $descricao = $_POST['descricao'];

						 //INFO IMAGEM
		$file 		= $_FILES['img'];
		$numFile	= count(array_filter($file['name']));

		//PASTA
		$folder		= '../upload/';

		//REQUISITOS
		$permite 	= array('image/jpeg', 'image/png');
		$maxSize	= 1024 * 1024 * 5;

		//MENSAGENS
		$msg		= array();
		$errorMsg	= array(
			1 => 'O arquivo no upload é maior do que o limite definido em upload_max_filesize no php.ini.',
			2 => 'O arquivo ultrapassa o limite de tamanho em MAX_FILE_SIZE que foi especificado no formulário HTML',
			3 => 'o upload do arquivo foi feito parcialmente',
			4 => 'Não foi feito o upload do arquivo'
		);

		if($numFile <= 0){
			$erroimg = '<div class="alert alert-danger alert-dismissable alert-style-1">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="zmdi zmdi-block"></i>Ops! Selecione uma Imagem e tente de novo!
  </div>';
		}
		else if($numFile >=2){
			echo '<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						Você ultrapassou o limite de upload. Selecione apenas uma foto e tente novamente!
					</div>';
		}else{
			for($i = 0; $i < $numFile; $i++){
				$name 	= $file['name'][$i];
				$type	= $file['type'][$i];
				$size	= $file['size'][$i];
				$error	= $file['error'][$i];
				$tmp	= $file['tmp_name'][$i];

				$extensao = @end(explode('.', $name));
				$novoNome = rand().".$extensao";

				if($error != 0)
					$msg[] = "<b>$name :</b> ".$errorMsg[$error];
				else if(!in_array($type, $permite))
					$msg[] = "<b>$name :</b> Erro imagem não suportada!";
				else if($size > $maxSize)
					$msg[] = "<b>$name :</b> Erro imagem ultrapassa o limite de 5MB";
				else{

					if(move_uploaded_file($tmp, $folder.'/'.$novoNome)){
						//$msg[] = "<b>$name :</b> Upload Realizado com Sucesso!";

						 $insert = "INSERT INTO tb_postagens (titulo, data, imagem, exibir, descricao) VALUES (:titulo, :data, :imagem, :exibir, :descricao)";
						try{
						$result = $connection->prepare($insert);
						$result->bindParam(':titulo', $titulo, PDO::PARAM_STR);
						$result->bindParam(':data', $data, PDO::PARAM_STR);
						$result->bindParam(':imagem', $novoNome, PDO::PARAM_STR);
						$result->bindParam(':exibir', $exibir, PDO::PARAM_STR);
						$result->bindParam(':descricao', $descricao, PDO::PARAM_STR);
						$result->execute();
						$contar = $result->rowCount();
						if($contar>0){

							$postsucesso = '<div class="alert alert-success alert-dismissable alert-style-1">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="zmdi zmdi-check"></i>Yay! Post Cadastrado com sucesso
        </div>';
							}else{
							$posterro = '<div class="alert alert-danger alert-dismissable alert-style-1">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="zmdi zmdi-block"></i>Ops! Não foi possível cadastrar o post.
          </div>';
							}
							}catch(PDOException $e){
							echo $e;
							}

					}else
						$msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";

				}

				foreach($msg as $pop)
				echo '';
					//echo $pop.'<br>';
			}
		}




							 }

						?>

            <form id="edit-profile"  action="#" method="POST" enctype="multipart/form-data"><br>
                            <?php echo $erroimg; ?>
                            <?php echo $postsucesso; ?>
                            <?php echo $posterro; ?>
            <div class="form-group"><br>
              <label class="control-label mb-10 text-left">Título</label>
              <input type="text" class="form-control" id="titulo" value="" name="titulo">
            </div>
            <div class="form-group">
              <label class="control-label mb-10">Data</label>
              <input type="text" placeholder="" data-mask="99/99/9999" class="form-control" name="data">
            </div>
            <div class="form-group mb-30">
              <label class="control-label mb-10 text-left">Imagem</label>
              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Selecione</span> <span class="fileinput-exists btn-text">Mudar</span>
                <input type="file" id="imagem" name="img[]">
              </span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remover</span></a>
              </div>
            </div>
            <div class="form-group mt-30 mb-30">
              <label class="control-label mb-10 text-left">Disponível?</label>
              <select class="form-control" id="exibir" value="" name="exibir">
                <option>Sim</option>
                <option>Não</option>
              </select>
            </div>
            <div class="form-group">
              <label class="control-label mb-10 text-left">Descrição</label>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <textarea class="tinymce" name="descricao" id="descricao"></textarea>
                </div>
              </div>            </div>

              <input type="submit" class="btn btn-success btn-anim" name="cadastrar" value="Cadastrar">


          </form>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <!-- Footer -->
    <footer class="footer container-fluid pl-30 pr-30">
        <div class="row">
            <div class="col-sm-12">
                <p>2017 &copy; Jetson. Pampered by Hencework</p>
            </div>
        </div>
    </footer>
    <!-- /Footer -->
</div>
<!-- /Main Content -->
