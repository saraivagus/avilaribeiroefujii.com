<div class="page-wrapper">
    <div class="container-fluid pt-25">
        <div class="col-sm-12">
            <?php
                if(isset($_GET['acao'])){
                  $acao = $_GET['acao'];

                    if($acao=='welcome'){
                      echo
                 '<div class="alert alert-info alert-dismissable alert-style-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <i class="zmdi zmdi-info-outline"></i>Olá '.$nomeLogado.',</strong> bem-vindo ao sistema!
                                  </div>';
                    }
                }
                ?>
        </div>
        <!-- Row -->
        <?php
            // excluir post
            if(isset($_GET['delete'])){
              $id_delete = $_GET['delete'];

              // seleciona a imagem
              $seleciona = "SELECT * FROM tb_postagens WHERE id=:id_delete";
              try{
                $result = $connection->prepare($seleciona);
                $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
                $result->execute();
                $contar = $result->rowCount();
                if($contar>0){
                  $loop = $result->fetchAll();
                  foreach ($loop as $exibir){
                  }
                   $fotoDeleta = $exibir['imagem'];
                   $arquivo = "../upload/" .$fotoDeleta;
                   unlink($arquivo);


              // exclui o registro
                    $seleciona = "DELETE FROM tb_postagens WHERE id=:id_delete";
                    try{

                      $result = $connection->prepare($seleciona);
                      $result->bindParam('id_delete',$id_delete, PDO::PARAM_INT);
                      $result->execute();
                      $contar = $result->rowCount();
                      if($contar>0){
                        	echo '<div class="alert alert-success alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-check"></i>Yay! O post foi excluido com sucesso!
										</div>';
                      }else{
                        echo '<div class="alert alert-danger alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-block"></i>Post excluido com sucesso!
										</div>';
                      }
                        }catch (PDOWException $erro){ echo $erro;}

                }else{
            echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
            			  <strong>Erro!</strong> Post já excluido ou não existe.
                            		  </div>';
                }

              }catch (PDOWException $erro){ echo $erro;}



            }

            ?>

            <div class="col-sm-12">
							<div class="panel panel-primary card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-light">Últimas notícias</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p>Olá, como podem ver o sistema foi mudado para melhor performance!<br>
                    Para acessar seus arquivos, basta clicar na aba <strong>Arquivos > Visualizar</strong>!<br>
                    Qualquer dúvidas estou a disposição<br><br>
                    Atenciosamente,<br>
                    Gustavo Saraiva</p>
									</div>
								</div>
							</div>
						</div>
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-ligth">Últimos Posts</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap mt-40">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th>Data</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            include("includes/limita-texto.php");
                                                          $select = "SELECT * from tb_postagens ORDER BY id DESC LIMIT 10";
                                                          $contagem =1;
                                            try{
                                            	$result = $connection->prepare($select);
                                            	$result->execute();
                                            	$contar = $result->rowCount();
                                            	if($contar>0){
                                                  while($mostrar = $result->FETCH(PDO::FETCH_OBJ)){

                                                          ?>
                                        <tr>
                                            <td> <?php echo $mostrar->titulo;?> </td>
                                            <td> <?php echo $mostrar->data;?> </td>
                                            <td class="text-nowrap">
                                                <a href="EditarPostagem.php?id=<?php echo $mostrar->id;?>" class="mr-25" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                <a href="home.php?delete=<?php echo $mostrar->id;?>" data-toggle="tooltip" data-original-title="Close" onClick="return confirm('Deseja realmente excluir o post?')"> <i class="fa fa-close text-danger"></i> </a>
                                            </td>
                                        </tr>
                                        <?php
                                            }
                                            }else{
                                            echo '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Aviso</strong> Não existem posts cadastrados em nosso banco de dados.
                                                          </div>';
                                            }
                                            }catch(PDOException $e){
                                            echo $e;
                                            }
                                                      ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
    </div>
    <!-- Footer -->
    <footer class="footer container-fluid pl-30 pr-30">
        <div class="row">
            <div class="col-sm-12">
                <p>2017 &copy; Jetson. Pampered by Hencework</p>
            </div>
        </div>
    </footer>
    <!-- /Footer -->
</div>
<!-- /Main Content -->
