<script type="text/javascript">
jQuery(function($){
   $("#date").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
});
</script>
<div class="page-wrapper">
    <div class="container-fluid pt-25">


<div class="col-sm-12">
  <div class="panel panel-default card-view">
    <div class="panel-heading">
      <div class="pull-left">
        <h6 class="panel-title txt-dark">Cadastrar novo Post</h6>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-wrapper collapse in">
      <div class="panel-body">
        <div class="form-wrap">
          <?php


// RECUPERA DADOS
if(!isset($_GET['id'])){
    header("Location: home.php"); exit;
}
    $id = $_GET['id'];
                $select = "SELECT * from tb_postagens WHERE id=:id";
                $contagem =1;
		try{
			$result = $connection->prepare($select);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$contar = $result->rowCount();
			if($contar>0){
        while($mostrar = $result->FETCH(PDO::FETCH_OBJ)){
                $idPost = $mostrar->id;
                $titulo = $mostrar->titulo;
                $data = $mostrar->data;
                $imagem = $mostrar->imagem;
                $exibir = $mostrar->exibir;
                $descricao = $mostrar->descricao;

        }
			}else{
				echo '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>
					  <strong>Aviso</strong> Não existem dados cadastrados com o id informado.
                      </div>';exit;
			}
		}catch(PDOException $e){
			echo $e;
    }

$novoNome = $imagem;
                    // ATUALIZA
					 if(isset($_POST['atualizar'])){

						 $titulo 	= trim(strip_tags($_POST['titulo']));
						 $data 		= trim(strip_tags($_POST['data']));
						 $exibir 	= trim(strip_tags($_POST['exibir']));
						 $descricao = $_POST['descricao'];

						 if(!empty($_FILES['img']['name'])){


						 //INFO IMAGEM
		$file 		= $_FILES['img'];
		$numFile	= count(array_filter($file['name']));

		//PASTA
		$folder		= '../upload/';

		//REQUISITOS
		$permite 	= array('image/jpeg', 'image/png');
		$maxSize	= 1024 * 1024 * 5;

		//MENSAGENS
		$msg		= array();
		$errorMsg	= array(
			1 => 'O arquivo no upload é maior do que o limite definido em upload_max_filesize no php.ini.',
			2 => 'O arquivo ultrapassa o limite de tamanho em MAX_FILE_SIZE que foi especificado no formulário HTML',
			3 => 'o upload do arquivo foi feito parcialmente',
			4 => 'Não foi feito o upload do arquivo'
		);

		if($numFile <= 0){
			/* echo '<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						Selecione uma imagem e tente novamente!
					</div>'; */
		}
		else if($numFile >=2){
			echo '<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						Você ultrapassou o limite de upload. Selecione apenas uma foto e tente novamente!
					</div>';
		}else{
			for($i = 0; $i < $numFile; $i++){
				$name 	= $file['name'][$i];
				$type	= $file['type'][$i];
				$size	= $file['size'][$i];
				$error	= $file['error'][$i];
				$tmp	= $file['tmp_name'][$i];

				$extensao = @end(explode('.', $name));
				$novoNome = rand().".$extensao";

				if($error != 0)
					$msg[] = "<b>$name :</b> ".$errorMsg[$error];
				else if(!in_array($type, $permite))
					$msg[] = "<b>$name :</b> Erro imagem não suportada!";
				else if($size > $maxSize)
					$msg[] = "<b>$name :</b> Erro imagem ultrapassa o limite de 5MB";
				else{

					if(move_uploaded_file($tmp, $folder.'/'.$novoNome)){
						//$msg[] = "<b>$name :</b> Upload Realizado com Sucesso!";
						$arquivo = "../upload/" .$imagem;
						unlink($arquivo);

						}else
						$msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";

				}

				foreach($msg as $pop)
				echo '';
					//echo $pop.'<br>';
			}
		}

				}
				else {

$novoNome = $imagem;
				}

						$update = "UPDATE tb_postagens SET titulo=:titulo, data=:data, imagem=:imagem, exibir=:exibir, descricao=:descricao WHERE id=:id";

						try{
						$result = $connection->prepare($update);
						$result->bindParam(':id', $id, PDO::PARAM_INT);
						$result->bindParam(':titulo', $titulo, PDO::PARAM_STR);
						$result->bindParam(':data', $data, PDO::PARAM_STR);
						$result->bindParam(':imagem', $novoNome, PDO::PARAM_STR);
						$result->bindParam(':exibir', $exibir, PDO::PARAM_STR);
						$result->bindParam(':descricao', $descricao, PDO::PARAM_STR);
						$result->execute();
						$contar = $result->rowCount();
						if($contar>0){

							$AtualizaSucesso = '<div class="alert alert-success alert-dismissable alert-style-1">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="zmdi zmdi-check"></i>Yay! Post Atualizado com sucesso
        </div>';
							}else{
							$AtualizaErro = '<div class="alert alert-danger alert-dismissable alert-style-1">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="zmdi zmdi-block"></i>Ops! Não foi possível atualizar o post.
          </div>';
							}
							}catch(PDOException $e){
							echo $e;
							}



							 }

						?>

            <form id="edit-profile"  action="#" method="POST" enctype="multipart/form-data"><br>
                            <?php echo $erroimg; ?>
                            <?php echo $AtualizaSucesso; ?>
                            <?php echo $AtualizaErro; ?>
            <div class="form-group"><br>
              <label class="control-label mb-10 text-left">Título</label>
              <input type="text" class="form-control" id="titulo" value="<?php echo $titulo;?>" name="titulo">
            </div>
            <div class="form-group">
              <label class="control-label mb-10">Data</label>
              <input type="text" placeholder="" data-mask="99/99/9999" class="form-control" name="data" value="<?php echo $data;?>">
            </div>
            <div class="form-group mb-30">
              <label class="control-label mb-10 text-left">Imagem</label>
              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Selecione</span> <span class="fileinput-exists btn-text">Mudar</span>
                <input type="file" id="imagem" name="img[]">
              </span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remover</span></a>
              </div>
               <img src="../upload/<?php echo $novoNome;?>" width="80">
            </div>
            <div class="form-group mt-30 mb-30">
              <label class="control-label mb-10 text-left">Disponível?</label>
              <select class="form-control" id="exibir" value="" name="exibir">
                <option selected><?php echo $exibir;?></option>
													<?php if($exibir!='Sim'){ echo '<option>Sim</option>';}?>
                                                    <?php if($exibir!='Não'){ echo '<option>Não</option>';}?>
              </select>
            </div>
            <div class="form-group">
              <label class="control-label mb-10 text-left">Descrição</label>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <textarea class="tinymce" name="descricao" id="descricao"><?php echo $descricao;?></textarea>
                </div>
              </div>            </div>

              <input type="submit" class="btn btn-success btn-anim" name="atualizar" value="Atualizar">


          </form>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <!-- Footer -->
    <footer class="footer container-fluid pl-30 pr-30">
        <div class="row">
            <div class="col-sm-12">
                <p>2017 &copy; Jetson. Pampered by Hencework</p>
            </div>
        </div>
    </footer>
    <!-- /Footer -->
</div>
<!-- /Main Content -->
