<?php
ob_start();
session_start();
if(!isset($_SESSION['usuariosistema']) && !isset($_SESSION['senhasistema'])){
	header("Location:index.php?acao=negado");exit;
}
include("conecta.php");
include("includes/Logout.php");

$usuarioLogado = $_SESSION['usuariosistema'];
$senhaLogado = $_SESSION['senhasistema'];

	// seleciona a usuario logado

      $selecionaLogado = "SELECT * FROM login WHERE usuario=:usuarioLogado AND senha=:senhaLogado";
      try{
        $result = $connection->prepare($selecionaLogado);
        $result->bindParam('usuarioLogado',$usuarioLogado, PDO::PARAM_STR);
        $result->bindParam('senhaLogado',$senhaLogado, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
         if($contar =1){
          $loop = $result->fetchAll();
          foreach ($loop as $show){

                $nomeLogado  = $show['nome'];
                $userLogado  = $show['usuario'];
                $emailLogado = $show['email'];
                $passLogado  = $show['senha'];
                $nivelLogado = $show['nivel'];

          }
         }

        }catch (PDOWException $erro){ echo $erro;}
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Ávila Ribero E Fujii - Sistema Integrado</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>
